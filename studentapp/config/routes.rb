Rails.application.routes.draw do
  devise_for :users
  resources :students
  resources :groups
  resources :marks
  root 'students#index' 
end
