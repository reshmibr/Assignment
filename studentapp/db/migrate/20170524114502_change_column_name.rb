class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change

    rename_column :marks, :students_id, :student_id
    rename_column :marks, :subjects_id, :subject_id
  
  end
end
