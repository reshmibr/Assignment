class CreateGroupsStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :groups_students do |t|
    	t.integer :group_id
    	t.integer :student_id
    end
  end
end
