
 RSpec.describe SubjectsController, type: :controller do
 	before(:each) do
    @subject = FactoryGirl.create(:subject1)

  end
  it 'get new template' do
    get :new
    expect(response.status).to eq(302)
  end

  it 'creates subject' do 
  	subject_params = FactoryGirl.attributes_for(:subject1)  
  	expect { Subject.create(subject_params) }.to change{ Subject.count }.by(1)
	end
end
