require 'spec_helper'

RSpec.describe MarksController, type: :controller do
  it 'get new template' do
    expect{ (get :new).response.status.to render_template('new') }
  end
  it 'creates marks' do 
  	student = FactoryGirl.create(:student)
  	subject = FactoryGirl.create(:subject)  	
    expect { FactoryGirl.create(:mark1,student_id: student.id,subject_id: subject.id)}.to change{ Mark.count }.by(1)    
	end
end
