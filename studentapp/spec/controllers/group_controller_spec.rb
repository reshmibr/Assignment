require 'spec_helper'

RSpec.describe GroupsController, type: :controller do
  before(:each) do
    @group = FactoryGirl.create(:group1)
    @student = FactoryGirl.create(:student1)   
  end
  it 'get new template' do
    expect{ (get :new).response.status.to render_template('new') }
  end
  it 'creates group' do 
    @stud_params = FactoryGirl.attributes_for(:student1) 
  end
  it 'create group and students details' do
    if !Group.find_by_name("new_group")
      @group_data = FactoryGirl.attributes_for(:group1)
    else
      @group_data = @group
    end
    expect { Group.create(@group_data) }.to change{ Group.count }.by(1)
    @group.students << @student
  end
end
