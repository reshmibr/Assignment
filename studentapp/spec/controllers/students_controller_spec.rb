require 'spec_helper'

RSpec.describe StudentsController, type: :controller do
 	render_views
  before(:each) do
    @user = User.new(email: 'mail_admin@test.com', password: 'password_admin', password_confirmation: 'password_admin')
    @user.save
  end
  it 'render index template' do
    expect{ (get :index).response.status.to render_template('index') }
  end
  it 'get new template' do
    expect{ (get :new).response.status.to render_template('new') }
  end
  it 'create student record' do 
    student_params = FactoryGirl.attributes_for(:student1)       
    expect { Student.create(student_params) }.to change{ Student.count }.by(1)
  end
  it 'render show' do
    student = FactoryGirl.create(:student)  
    expect{ (get :show, id: student.id).response.status.to render_template('show') }
  end
end
