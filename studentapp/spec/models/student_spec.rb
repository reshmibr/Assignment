require 'spec_helper'
 RSpec.describe Student, type: :model do
  it 'calculate weighted average' do		 
    student = FactoryGirl.create(:student)
    subject = FactoryGirl.create(:subject)
    subject2 = FactoryGirl.create(:subject2)
    subject3 = FactoryGirl.create(:subject3)
    subject4 = FactoryGirl.create(:subject4)  
    mark1 = FactoryGirl.create(:mark1,student_id: student.id,subject_id: subject.id)
    mark2 = FactoryGirl.create(:mark2,student_id: student.id,subject_id: subject2.id)
    mark3 = FactoryGirl.create(:mark3,student_id: student.id,subject_id: subject3.id)
    mark4 = FactoryGirl.create(:mark4,student_id: student.id,subject_id: subject4.id)
    weighted_average = student.weighted_average(student)   
    expect(weighted_average[1]).to eq(12.5) 
  end
end
