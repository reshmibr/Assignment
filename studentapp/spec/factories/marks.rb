require 'rails_helper'

FactoryGirl.define do
	factory :mark1, class: Mark do
  	mark 50
  	subject_id 1
  	student_id 1
  end  
	factory :mark2, class: Mark do
  	mark 50
  	subject_id 2
  	student_id 2
  end  
  factory :mark3, class: Mark do
    mark 50
    subject_id 3
    student_id 2
  end  
  factory :mark4, class: Mark do
    mark 50
    subject_id 1
    student_id 2
  end  
  factory :mark5, class: Mark do
    mark 50
    subject_id 4
    student_id 2
  end  
end
