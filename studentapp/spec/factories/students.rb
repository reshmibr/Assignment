require 'spec_helper'

FactoryGirl.define do
	factory :student1, class: Student do
    sequence(:name) { |n|  "name#{n}" }
    sequence(:email) { |n| "email#{n}@example.com" }
  end
  factory :student, class: Student do
    name "reshmi"
    email "resh@example.com"
  end
  factory :student2, class: Student do
    name "reshmi"
    email "reshmi@example.com"
  end
end
