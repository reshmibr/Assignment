require 'rails_helper'

FactoryGirl.define do
	factory :subject, class: Subject do
  	name "Physics"
	end
	factory :subject2, class: Subject do
  	name "Chemistry"
	end
	factory :subject3, class: Subject do
  	name "Maths"
	end
	factory :subject4, class: Subject do
  	name "English"
	end  
end
