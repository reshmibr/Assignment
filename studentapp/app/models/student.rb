class Student < ApplicationRecord
	validates :name, presence: true
	validates :email, :presence => true, :uniqueness => true                      
	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
	has_many :marks
	has_many :groups, through: :group_students
	has_many :group_students
	#method for calculating weighted average
	def weighted_average(student) 
		mark_array = []
		student.marks.each do |f|
			if f.subject.name == "Physics"		
				@weighted_phy = f.mark * 0.30
				mark_array << (f.mark * 0.30).round(2)
			elsif f.subject.name == "Chemistry"				
				@weighted_che = f.mark * 0.20
				mark_array << (f.mark * 0.20).round(2)
			elsif f.subject.name == "Maths"				
				@weighted_maths = (f.mark * 0.40)
				mark_array << (f.mark * 0.40).round(3)
			else
				@weighted_english = f.mark * 0.10
				mark_array << (f.mark * 0.10).round(3)
			end
		end
		@total_weighted_average = (@weighted_phy.to_f + @weighted_che.to_f + @weighted_maths.to_f + @weighted_english.to_f ) / 4	
		return mark_array, @total_weighted_average
	end	
end
