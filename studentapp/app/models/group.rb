class Group < ApplicationRecord
	has_many :group_students
	accepts_nested_attributes_for :group_students 
	has_many :students,through: :group_students	
end
