class StudentsController < ApplicationController
	def index
		@students = Student.all		
	end
	def new
		@student = Student.new
		@students = Student.all		
	end
	def create
		@students = Student.all	
		@student = Student.create(student_params)
		respond_to do |format|
			if @student.save
        format.js
     	else
      	format.html { render 'new', notice: 'Student  Not Added' }
      	format.json { head :no_content }
    	end
    end
	end	
	def show	
		@student = Student.find(params[:id])		
		@marks_array, @total_weighted_average = @student.weighted_average(@student)	
	end

private
	def student_params
		params.require(:student).permit(:name,:email)
	end
end
