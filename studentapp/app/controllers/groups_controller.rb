class GroupsController < ApplicationController
	def new
		@group = Group.new
	end
	def create
		if !Group.find_by_name(params[:group][:name])
			@group = Group.create(params_of_group)
		else
			@group = Group.find_by_name(params[:group][:name])
		end
		@group_data = Group.find(@group.id)
		student_details = Student.find(params[:group][:students][:students_id])
		respond_to do |format|
			if @group_data.students << student_details
				format.html { redirect_to new_group_path, notice: 'Group Created' }
      	format.json { head :no_content }
      else
      	format.html { render action: 'new' }
      	format.json { render json: @group_data.errors, status: :unprocessable_entity }
      end
    end
	end
	
	private
		def params_of_group
			params.require(:group).permit(:name,:students)
		end
end
