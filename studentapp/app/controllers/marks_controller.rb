class MarksController < ApplicationController
	def new
		@mark = Mark.new
	end
	def create		
		@mark = Mark.new(mark_params)
		respond_to do |format|
			if @mark.save
      	format.html { redirect_to new_mark_path, notice: 'Mark Added' }
      	format.json { head :no_content }
    	else
      	format.html { render action: 'new' }
      	format.json { render json: @mark.errors, status: :unprocessable_entity }
    	end
    end
	end
	
private
	def mark_params
		params.require(:mark).permit(:student_id,:subject_id,:mark)
	end
end
